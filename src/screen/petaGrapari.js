import React, { Component } from 'react';
import { Platform, Text, View, StyleSheet, AsyncStorage } from 'react-native';
import MapView from 'react-native-maps';
import { Container, Content } from 'native-base';
import { AppHeader, AppFooter } from "../app-nav/index";

const GEOLOCATION_OPTIONS = { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 };
var Analytics = require('react-native-firebase-analytics');

export default class PetaGrapari extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jsonData: '',
            markers: [],
            markersWifiId: [],
            titlePeta: this.props.navigation.state.params.titlePeta
        }
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: 0.4022,
                        longitudeDelta: 0.0021,
                    }
                })
                if (this.state.titlePeta == "Grapari") {
                    fetch("http://ec2-54-255-226-10.ap-southeast-1.compute.amazonaws.com:9009/api/plaza-all", {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                        },
                        body: JSON.stringify({
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude
                        })
                    })
                        .then(response => response.json())
                        .then((data) => {
                            this.setState({ markers: data.data.data });
                        });

                    Analytics.logEvent('peta_grapari', {
                        'item_id': 'peta_grapari'
                    });

                } else {
                    fetch("https://caramel.wifi.id/api/nearby/v1/wico", {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'X-API-KEY': 'c99c8fc2-a473-4c42-a755-05702f07820b'
                        },
                        body: JSON.stringify({
                            lon: position.coords.longitude,
                            lat: position.coords.latitude,
                            radius: 10000
                        })
                    })
                        .then(response => response.json())
                        .then((data) => {
                            this.setState({ markersWifiId: data });
                        });

                    Analytics.logEvent('peta_wifi_id', {
                        'item_id': 'peta_wifi_id'
                    });
                }

            });
    }

    render() {
        return (
            <Container>
                <AppHeader navigation={this.props.navigation} title={this.state.titlePeta} />
                <Content>
                </Content>
                <View style={styles.container}>
                    <MapView
                        style={styles.map}
                        showsUserLocation={true}
                        region={this.state.region}                   >
                        {this.state.titlePeta == "Grapari" ?
                            this.state.markers != null ?
                                this.state.markers.map((item, idx) => {
                                    return (
                                        <MapView.Marker key={idx}
                                            coordinate={{
                                                latitude: item.Latitude,
                                                longitude: item.Longitude
                                            }}
                                            title={item.Nama}
                                            description={item.Alamat}
                                        />
                                    )
                                }) : null :
                            this.state.markersWifiId != null ?
                                this.state.markersWifiId.map((item, idx) => {
                                    return (
                                        <MapView.Marker key={idx}
                                            coordinate={{
                                                latitude: Number(item.geometry.coordinates[0]),
                                                longitude: Number(item.geometry.coordinates[1])
                                            }}
                                            title={item.properties.name}
                                            description={item.properties.location}
                                        />
                                    )
                                }) : null}
                    </MapView>
                </View>
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    baseText: {
        fontFamily: 'Ubuntu-Light'
    },
    titleText: {
        fontFamily: 'Ubuntu-Regular',
        fontSize: 30,
        fontWeight: 'bold',
    },
    contentContainer: {
        borderWidth: 0,
        borderColor: '#FFF',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        backgroundColor: "#fff"
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 40
    },
    container: {
        backgroundColor: "#FFF",
        position: 'absolute',
        top: 80,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
});