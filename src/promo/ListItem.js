import React, { Component } from "react";
import { View, Image, StyleSheet, AsyncStorage, ActivityIndicator, TouchableHighlight } from "react-native";
import { Container, Content, Picker, Button, Text } from "native-base";



export default class ListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { itemWidth } = this.props
        return (
            <View style={{ margin:5, width: itemWidth, height: 200 }}>
                <Image resizeMethod="resize" style={{ width: itemWidth, height: 200 }} source={{ uri: this.props.image }} />
            </View>
        )
    }
}

